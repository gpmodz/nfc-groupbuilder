NFC-Groupbuilder
================

A Groupbuilding tool for Android using NFC and Email Communication. 

Based on an implementation which was a part of the PEOPLE-Project at the University of Bremen. 

Needed:
- Android 4.0.3 (API-Level 15)
- Java 1.6

To compile simply clone the project.